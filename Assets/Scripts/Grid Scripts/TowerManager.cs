﻿using UnityEngine;
using System.Collections.Generic;

/*
 * Maily keeps track of the towers - and also match settings
 * Since there is only one Game manger instance per scene, this has been made as a singleton
 * 
*/
public class TowerManager : MonoBehaviour {

	public static TowerManager singleton; 

	void Awake()
	{
		if (singleton != null) {
			Debug.LogError ("More than one Game manager is running"); 
		} else {
			singleton = this; 
			//singletons make it very easy to gain access to the Gamemanager without any gameobject.find or similar things. 
		}
	}

	#region tower tracking 
	private const string TOWER_ID_PREFIX = "Tower "; 

	public static Dictionary<string, Tower> towers = new Dictionary<string, Tower>();


	/*
	 * This is so each tower adds the id from network idenity to its name so that
	 * it is displayed. RegisterTower can be used to create a dictionary of all the
	 * towers in the session
	 * 
	*/
	public static void RegisterTower(string _netID, Tower _tower){
		string _towerID = TOWER_ID_PREFIX + _netID; 
		towers.Add (_towerID, _tower);
		_tower.transform.name = _towerID; //sets the name of the game object to the ID
	}

	public static void DeRegisterTower(string _towerID){
		towers.Remove (_towerID);
	}


	public static string FindIDFromTower(Tower _tower){
		foreach (KeyValuePair<string, Tower> pair in towers) {
			if (_tower = pair.Value) {
				return pair.Key;
			}
		}
		return null;
	}

	public static bool ContainsTower(Tower _tower){
		if (towers.ContainsValue (_tower)) {
			return true;
		}
		return false;
	}

	public static Tower GetTower(string _towerID)
	{
		return towers[_towerID];
	}

	//Find the closest tower to position
	public static Tower GetClosestTower(Vector3 position){
		Tower currentClosest = default(Tower);
		float currentClosestDistance = 0f;
		foreach (Tower tower in towers.Values) {
			float distance = Vector3.Distance (tower.gameObject.transform.position, position);
			if(distance < currentClosestDistance || currentClosestDistance == 0f){
				currentClosest = tower;
				currentClosestDistance = distance;
			}
		}
		if (currentClosest != default(Tower)) {
			return currentClosest;
		}
		Debug.LogError ("No tower found. Returned default tower likely breaking script using this method");
		return default(Tower);
	}



	//if you want to visualise the dictionary of towers it can be done from this script
	//I havent done it but if you want me to, I can work on it

	//This is just for testing - SHOULD BE COMMENTED OUT
	/*
	void OnGUI () note: found bug... the gun seems to be tower 1. O.o
	{
	    GUILayout.BeginArea(new Rect(200, 200, 200, 500));
	    GUILayout.BeginVertical();

	    foreach (string _towerID in towers.Keys)
	    {
	        GUILayout.Label(_towerID + "  -  " + towers[_towerID].transform.name);
	    }

	    GUILayout.EndVertical();
	    GUILayout.EndArea();
	}
	*/ 

	#endregion



}
