﻿using UnityEngine;
using UnityEngine.Networking;

using UnityEngine.Networking.NetworkSystem;
using System.Collections;
using System.Collections.Generic;
public class GridInteraction : NetworkBehaviour {
	Tower towerSlot;
	GameObject towerPanel;
	Camera mainCamera;
	GameObject wireBox;


	Player player;
	public Material wireBoxMat;
	public bool gridInteractionEnabled;
	[SerializeField]
	List<Tower> towerTypes;
	List<Vector3> currentNodes;
	int selectedTowerTypeIndex = 0;
	private Tower _currentTower;
	private Tower towerToDestroy;
	public Tower currentTower {
		get { 
			_currentTower = towerTypes [selectedTowerTypeIndex];
			return _currentTower;
		}
		set { currentTower = value; }
	}
	Vector3 spawnPos;
	public GameObject objToSpawn;
	Vector3 instanPos = Vector3.zero;
	RaycastHit hit;
	EnemySpawner spawnBehaviour;

	void Start () {
		currentNodes = new List<Vector3>();
		player = GetComponent<Player> ();
		gridInteractionEnabled = false;

		if (isClient) {
			mainCamera = FindCamera();
			wireBox = GameObject.CreatePrimitive (PrimitiveType.Cube);
			wireBox.GetComponent<MeshRenderer> ().material = wireBoxMat;
			wireBox.GetComponent<BoxCollider> ().enabled = false;
			wireBox.SetActive (false);
			wireBox.transform.localScale = new Vector3 (Grid.nodeRadius * 4, .3f, Grid.nodeRadius * 4);
		}
	}

	//void SetCurrentNodes(List<Vector3> _nodes){
	//}
	// Update is called once per frame
	void Update () {
		if (!isLocalPlayer)
			return;
		if (GameManager.state != GameManager.GameState.PHASE_BUILDING) {
			gridInteractionEnabled = false;
			wireBox.SetActive (false);
			return;
		}
		if (towerSlot != null) {
			towerSlot.prefab.transform.position = mainCamera.ScreenPointToRay (Input.mousePosition).direction * 4;
		}
		if (Input.anyKeyDown) {
			GetInput ();
		}
		if (gridInteractionEnabled) {

			hit = new RaycastHit (); 
			// We need to actually hit an object
			if (
				!Physics.Raycast (mainCamera.ScreenPointToRay (Input.mousePosition).origin,
					mainCamera.ScreenPointToRay (Input.mousePosition).direction, out hit, 4,
					Physics.DefaultRaycastLayers)) {
				wireBox.SetActive (false);
				return;
			}
			// Check that we are hitting a gameobject with tag Ground
			if (hit.collider.gameObject.tag != "Ground") {
				wireBox.SetActive (false);
				return;
			}

			GetCurrentNodes();
		
			wireBox.SetActive (true);
			wireBox.transform.position = instanPos;
		} else {
			if (wireBox.activeSelf) {
				wireBox.SetActive (false);
			}
		}
	}
	void GetCurrentNodes(){
		if (Grid.GetNode (hit.point) != null) {
			instanPos = GetMiddleOfNodes (hit.point);
			currentNodes = GetGridNodes (instanPos, Grid.nodeRadius);
		}
	}

	private Camera FindCamera()
	{
		if (GetComponent<Camera>())
		{
			return GetComponent<Camera>();
		}

		return Camera.main;
	}
	private List<Vector3> GetGridNodes(Vector3 _instanPos, float nodeRadius){
		List<Vector3> nodes = new List<Vector3> ();
		Vector3 nodePos = Vector3.zero;

		nodePos.x = _instanPos.x + nodeRadius;
		nodePos.z = _instanPos.z + nodeRadius;
		nodes.Add (nodePos);
		nodePos.x = _instanPos.x + nodeRadius;
		nodePos.z = _instanPos.z - nodeRadius;
		nodes.Add (nodePos);
		nodePos.x = _instanPos.x - nodeRadius;
		nodePos.z = _instanPos.z + nodeRadius;
		nodes.Add (nodePos);
		nodePos.x = _instanPos.x - nodeRadius;
		nodePos.z = _instanPos.z - nodeRadius;
		nodes.Add (nodePos);

		return nodes;
	}
	private Vector3 GetMiddleOfNodes(Vector3 hit){
		GridNode currentNode = Grid.GetNode (hit);
		Vector3 offset = hit - currentNode.worldPos;
		Vector3 middle = currentNode.worldPos;
		if (offset.x >= 0 && offset.z >= 0) {
			middle.x += Grid.nodeRadius;
			middle.z += Grid.nodeRadius;
		}else if (offset.x >= 0 && offset.z < 0) {
			middle.x += Grid.nodeRadius;
			middle.z -= Grid.nodeRadius;
		}else if (offset.x < 0 && offset.z >= 0) {
			middle.x -= Grid.nodeRadius;
			middle.z += Grid.nodeRadius;
		}else if (offset.x < 0 && offset.z < 0) {
			middle.x -= Grid.nodeRadius;
			middle.z -= Grid.nodeRadius;
		}

		return middle;
	}
	public static void DestroyTower(string _towerID){
		Destroy (TowerManager.towers [_towerID].prefab);
		TowerManager.DeRegisterTower (_towerID);
	}
	public void DestroyTower(Tower _tower){
		if (isServer) {
			string ID = TowerManager.FindIDFromTower (_tower);
			if (ID == null) {
				Debug.LogError ("Tower not in the dictionary");
				return;
			}

			Destroy (_tower.prefab);
			TowerManager.DeRegisterTower (ID);
		} else {
			CmdDestroyTower ();
		}

	}
	[Command]
	void CmdDebug(string _msg){
		Debug.Log (_msg);
	}
	[Command]
	void CmdDestroyTower(){
		string ID = TowerManager.FindIDFromTower (towerToDestroy);
		if (ID == null) {
			Debug.LogError ("Tower not in the dictionary");
			return;
		}

		Debug.LogError (towerToDestroy.name);
		Destroy (towerToDestroy.prefab);
		TowerManager.DeRegisterTower (ID);
	}
	private void PlaceTower(Tower  _spawnTower, List<Vector3> _nodes, Vector3 _instanPos){
		if (!IsWalkableNodes (_nodes)) {
			Debug.Log ("Cannot place tower here as one or more nodes are unwalkable");
			return;	
		}
		if (_spawnTower.price > player.funds) {
			Debug.Log ("Cannot place tower as not enough funds");
			return;
		}

		_spawnTower.nodes = _nodes;
		player.funds -= _spawnTower.price;
		SpawnTower(_spawnTower, instanPos);
	}
	public void SpawnTower(Tower _tower, Vector3 _pos){
		if (!player.isLocalPlayer) {
			return;
		}
		objToSpawn = _tower.prefab;

		if (isServer) {
			SetUnwalkableNodes (_tower.nodes);
			GameObject _obj = (GameObject)Instantiate (objToSpawn, _pos, Quaternion.identity);
			NetworkServer.Spawn(_obj);
			TowerManager.RegisterTower (_obj.GetComponent<NetworkIdentity> ().netId.ToString(), _obj.GetComponent<Tower>());
		}else{
			CmdSpawnObject (instanPos);
		}
	}
	[Command]
	void CmdSpawnObject(Vector3 _centre){
		SetUnwalkableNodes (GetGridNodes(_centre, Grid.nodeRadius));
		GameObject _obj = (GameObject)Instantiate (objToSpawn, _centre, Quaternion.identity);
		NetworkServer.Spawn(_obj);
		TowerManager.RegisterTower (_obj.GetComponent<NetworkIdentity> ().netId.ToString(), _obj.GetComponent<Tower>());
	}
	private bool IsWalkableNodes(List<Vector3> nodes){
		foreach (Vector3 node in nodes) {
			if (!Grid.GetNode (node).walkable) {
				return false;
			}
		}
		return true;
	}
	private void SetUnwalkableNodes(List<Vector3> nodes){
		foreach (Vector3 node in nodes) {
			Vector2 gn = Grid.GetVector2 (node);
			Grid.SetUnwalkable((int)gn.x, (int)gn.y);
		}
	}


	private void GetInput(){
		
		if (Input.GetKeyDown (KeyCode.I)) {
			gridInteractionEnabled = !gridInteractionEnabled;
			wireBox.SetActive (!wireBox.activeSelf);
		}
		if(gridInteractionEnabled){
			//select tower type
			for (int i = 0; i < towerTypes.Count - 1; i++) {
				if (Input.GetKeyDown (i.ToString())) {
					selectedTowerTypeIndex = i;
				}
			}
			if (Input.GetMouseButtonDown (0) && wireBox.activeSelf) {
				if (IsWalkableNodes(currentNodes) && player.funds >= towerTypes[selectedTowerTypeIndex].price) {
					if (towerTypes[selectedTowerTypeIndex].SameType("Trigun")) {
						PlaceTower (currentTower, currentNodes, instanPos);
					}	
				}
			}

			if (Input.GetMouseButtonDown (1)) {
				if (Physics.Raycast (mainCamera.ScreenPointToRay (Input.mousePosition).origin,
					mainCamera.ScreenPointToRay (Input.mousePosition).direction, out hit, 4,
					Physics.DefaultRaycastLayers)) {
					if (hit.collider.gameObject.tag == "Tower") {
						Tower _tower = hit.collider.gameObject.GetComponent<Tower> ();
						if (TowerManager.ContainsTower (_tower)) {
							player.funds += _tower.price / 2;
							foreach (Vector3 tn in _tower.nodes) {
								Grid.GetNode (tn).SetWalkable(true);
							}
							DestroyTower (_tower);
						}
					}
				}
			}
			if (Input.GetKeyDown (KeyCode.Z)) {
				if (towerSlot == null) {
					if (hit.collider.gameObject.tag == "Tower") {
						Tower _tower = hit.collider.gameObject.GetComponent<Tower> ();
						if (TowerManager.ContainsTower (_tower)) {
							_tower.prefab.GetComponent<BoxCollider> ().enabled = false;
							towerSlot = _tower;
							DestroyTower (_tower);
						}
					}
				}else {
					if (Grid.GetNode (hit.point) != null) {
						PlaceTower (towerSlot, currentNodes, GetMiddleOfNodes (hit.point));
					}
				}
			}
		}
	}
}
