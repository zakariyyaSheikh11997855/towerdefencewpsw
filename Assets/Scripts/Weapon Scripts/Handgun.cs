﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Handgun : BaseWeapon {

	GameObject ch;
	Animation gunAnim;
	AudioSource gunSound;
	bool isFiring = false;
	bool isADS = false;

	public int currentAmmo; 
	public int currentExtraAmmo;
	public int maxClipSize;
	public int maxExtraAmmo;

	// Use this for initialization
	void Start ()
	{	
		gunAnim = GetComponent<Animation> ();
		gunAnim.Play ("Draw");
		ch = GameObject.FindGameObjectWithTag ("Crosshair");
		ch.SetActive (true);
		gunSound = GetComponent<AudioSource> ();

		currentAmmo = 30; 
		currentExtraAmmo = 30;
		maxClipSize = 30;
		maxExtraAmmo = 30;

		Debug.Log ("Trying ...");
		Text[]texts = AmmoDisplay.GetComponentsInChildren<Text>();
		Debug.Log ("Trying ... the arry is" + texts.Length);
		foreach (Text text in texts) {
			Debug.Log ("Trying to find that panel ");
			if (text.name == "ExtraAmmo") {
				Debug.Log ("Yup found it");
				AmmoText = text.GetComponent<Text>();
			}
		}

	}

	// Update is called once per frame
	void Update ()
	{
		if (!gunAnim.isPlaying) {
			//Debug.Log ("ANIMATION IS NOT PLAYING");
			if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0))) {
				isFiring = true;
			} else if ((Input.GetButtonUp ("Fire1") || Input.GetMouseButtonUp (0))) {
				isFiring = false;
			}


			if (Input.GetButtonDown ("Fire2")) {
				if (isADS) {
					gunAnim.Play ("UnADS");
					isADS = false;
				} else {
					gunAnim.Play ("ADS");
					isADS = true;
				}
			}


			if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0)) && currentAmmo > 0) {
				AudioSource gunSound = GetComponent<AudioSource> ();
				if (isADS) {
					gunAnim.Play ("ADSFire");
				} else {
					gunAnim.Play ("Hipfire");
				}
				gunSound.Play ();

				Debug.Log ("1Ammo is now " + currentAmmo);
				currentAmmo--;
				Debug.Log ("2Ammo is now " + currentAmmo);
				Debug.Log (AmmoText);

				AmmoText.GetComponent<Text> ().text = "" + currentAmmo.ToString();//+ "/ " + currentExtraAmmo;


			}
		}


	}
}
