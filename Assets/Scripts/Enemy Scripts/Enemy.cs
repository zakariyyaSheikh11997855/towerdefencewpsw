﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Enemy : BaseHealth {
	[SerializeField]//offsetToUpdatePath is how far the current target is away from the end of the current path in order to get a new path
	protected float weaponDamage, weaponRange, moveSpeed, offsetToUpdatePath; 
	protected int currentPathIndex = 0;
	protected int pathAheadCheck = 5;
	protected int enemyId;

	protected List<Vector2> path;
	protected Vector3 moveTarget;
	protected Vector3 targetOldPos;

	protected Pathfinder pathfinder;
	protected Transform playerBase;
	protected Transform currentTarget;

	public enum Target { PlayerBase, Player, Turret }
	private Target _target = Target.PlayerBase;
	public Target target {
		get { return _target; }
		set { _target = value; }
	}


	public void Start() {
		pathfinder = new Pathfinder();
		playerBase = GameObject.Find ("Base").transform;
		UpdatePath ();
		Physics.IgnoreCollision (GetComponent<BoxCollider> (), GameObject.FindGameObjectWithTag ("InWall").GetComponent<BoxCollider>());
	}

	protected void Update () {
		//if player moves or (add later if a tower is placed down) update path
		if (target == Target.Player) {
			if (Vector3.Distance(currentTarget.position, targetOldPos) > offsetToUpdatePath) {
				UpdatePath ();
			}
			FollowPath ();
			targetOldPos = currentTarget.position;
		}
	}

	protected void OnCollisionEnter(Collision collision){
		UpdatePath ();
	}
	protected override void Die (){
		Debug.Log (transform.name + " is Dead");
		Explode ();
	}
	protected void Explode() {
		if (Vector3.Distance (playerBase.position, transform.position) < weaponRange) {
			if (!GetComponent<ParticleSystem> ().isPlaying) {
				playerBase.GetComponent<Base> ().RpcTakeDamage (weaponDamage);
			}
		}
		List<Player> playersInRange = GameManager.GetPlayersInRange (transform.position, weaponRange);
		if (playersInRange.Count > 0) {
			foreach (Player player in playersInRange) {
				player.RpcTakeDamage (weaponDamage);
				Debug.Log ("Explosion hit " + player.transform.name);
			}
		}
		Destroy(gameObject);
		Debug.Log ("destory has been called for an objet");
	}

	protected void FollowPath(){
		if(path != null && currentPathIndex < path.Count){
			if (!CheckPathClear ()) {//TODO: UPDATE THIS TO ONLY RUN SOMETIMES TO IMPROVE PERFORMANCE
				UpdatePath ();
			}
			if(moveTarget != transform.position){
				CalculateMoveTarget();
			}
			else{
				currentPathIndex++;
				CalculateMoveTarget();
			}
			float step = moveSpeed * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, moveTarget, step);
		}
	}
	protected void CalculateMoveTarget(){
		moveTarget = Grid.GetWorldPos((int)path[currentPathIndex].x, (int)path[currentPathIndex].y);
		moveTarget = new Vector3(moveTarget.x, transform.position.y, moveTarget.z);
	}
	protected bool CheckPathClear(){
		for (int i = 0; currentPathIndex + i < path.Count && i < pathAheadCheck; i++) {
			if(!pathfinder.checkWalkable((int)path[currentPathIndex + i].x, (int)path[currentPathIndex + i].y)){
				return false;
			}
		}
		return true;
	}
	public void UpdatePath(){
		switch(target){
		case Target.Player:
			UpdatePath (GameManager.GetClosestPlayer(transform.position).gameObject.transform);
			break;
		case Target.PlayerBase:
			UpdatePath (playerBase);
			break;
		}
	}	
	protected void UpdatePath(Transform _newTarget){
		currentTarget = _newTarget;
		try{
			path = pathfinder.findPath (Grid.GetVector2 (transform.position), Grid.GetVector2 (_newTarget.position));
		}catch(Exception e){
			Debug.LogError ("The error below is expected. Doesn't seem to affect anything and I decided to move on after trying to fix for a while. If you want to attemp to fix it, first delete this try catch as it moves the error. - Riordan");

		}
		currentPathIndex = 0;
	}
}
