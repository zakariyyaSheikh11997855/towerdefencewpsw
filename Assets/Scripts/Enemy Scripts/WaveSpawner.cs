﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WaveSpawner : MonoBehaviour {

	public class Wave {
		public List<EnemyType> enemies;
		public Vector3 spawnPosition;
		public float spawnRadius;
		public float rate;

		public Wave(){

		}
		public Wave(List<EnemyType> enemies, Vector3 spawnPosition, float spawnRadius, float rate){
			this.enemies = enemies;
			this.spawnPosition = spawnPosition;
			this.spawnRadius = spawnRadius;
			this.rate = rate;
		}
	}

	public GameObject basicEnemy;
	public enum SpawnState { SPAWNING, WAITING };
	public SpawnState state;
	bool newWave = true;
	float readyTime = 5f;
	float spawnCountdown = 0;
	public Vector3 defaultSpawnPosition = Vector3.zero;
	public float defaultSpawnRadius = 5;
	public float defaultSpawnRate = 1;
	public List<Wave> waves;
	int nextWave = 0;
	EnemySpawner spawnBehaviour;
	public int tempSetupNoOfEnemies = 5;
	// Use this for initialization
	void Start () {
		spawnBehaviour = GetComponent<EnemySpawner> ();
		waves = new List<Wave> ();
		TempSetup (tempSetupNoOfEnemies);
		state = SpawnState.WAITING;

	}
	void TempSetup(int number){
		List<EnemyType> waveEnemies = new List<EnemyType> ();
		waveEnemies.Add (new EnemyType ("waveEnemy", Enemy.Target.Player, basicEnemy, number));

		Wave wave1 = new Wave (waveEnemies, defaultSpawnPosition, defaultSpawnRadius, defaultSpawnRate);
		waves.Add (wave1);
		waveEnemies.Add (new EnemyType ("waveEnemy", Enemy.Target.Player, basicEnemy, number));
		Wave wave2 = new Wave (waveEnemies, defaultSpawnPosition, defaultSpawnRadius, defaultSpawnRate);
		waves.Add (wave2);
	}
	// Update is called once per frame
	void Update () {
		if (GameManager.state == GameManager.GameState.PHASE_FIGHTING) {
			if(state != SpawnState.SPAWNING && newWave){
				newWave= false;
				StartCoroutine (SpawnEnemies(waves[nextWave]));
				nextWave++;
			} else if(state != SpawnState.SPAWNING && !IsEnemyAlive()){
				if (nextWave >= waves.Count) {
					GameManager.state = GameManager.GameState.WON;
				} else {
					newWave = true;
					GameManager.state = GameManager.GameState.PHASE_BUILDING;
					GameManager.nextPhaseCountdown = GameManager.buildTime;
				}
			}	
		}
	}

	IEnumerator SpawnEnemies(Wave wave){
		state = SpawnState.SPAWNING;
		for (int j = 0; j < wave.enemies.Count; j++) {
			for (int i = 0; i < wave.enemies[j].amount; i++) {
				spawnBehaviour.SpawnEnemy (wave.enemies[j].prefab, wave.enemies[j].target, wave.spawnPosition, wave.spawnRadius);
				yield return new WaitForSeconds (wave.rate);
			}
		}
		state = SpawnState.WAITING;

		yield break;
	}
	bool IsEnemyAlive(){
		return GameObject.FindGameObjectWithTag ("Enemy") != null ? true : false;
	}

	#region tempGridGizmosLocation
	void OnDrawGizmos(){
		if (Grid.singleton == null)
			return;
		if (Grid.grid != null && GameObject.FindGameObjectsWithTag ("Player").Length > 0) {
			//there are more than one players - how will this work with the Grid?
			List<GameObject> players = new List<GameObject>(GameObject.FindGameObjectsWithTag ("Player"));

			GridNode playerNode = Grid.GetNode (players[0].transform.position);

			foreach(GridNode gn in Grid.grid){
				Gizmos.color = gn.walkable ? Color.white : Color.red;
				Gizmos.color = (playerNode == gn) ? Color.green : Gizmos.color;

				Gizmos.DrawWireCube(gn.worldPos, new Vector3(Grid.nodeDiameter, 1, Grid.nodeDiameter));

			}
		}
	}
	#endregion
}
