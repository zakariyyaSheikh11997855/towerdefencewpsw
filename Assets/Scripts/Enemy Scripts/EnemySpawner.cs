﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class EnemySpawner : NetworkBehaviour {

	Vector3 spawnPos;
	public GameObject objToSpawn;
	public EnemyType enemy;
	private NetworkBehaviour player;
	Vector3 UpdateSpawnPos(Vector3 centreSpawnPos, float range){
		Vector3 spawnPos;
		spawnPos.x = centreSpawnPos.x + (Random.Range (-range, range));
		spawnPos.z = centreSpawnPos.z + (Random.Range (-range, range));
		spawnPos.y = centreSpawnPos.y;
		return spawnPos;
	}
	void Start(){
		SetupPlayer ();
	}
	void SetupPlayer(){
		foreach (GameObject p in GameObject.FindGameObjectsWithTag ("Player")) {
			if(p.GetComponent<NetworkIdentity>() != null){
				if (p.GetComponent<NetworkIdentity> ().isLocalPlayer) {
					player = p.GetComponent<Player> ();
				}
			}
		}
	}
	void Update(){
		if (Input.GetKeyDown(KeyCode.F))
		{
			SpawnEnemy (objToSpawn, Enemy.Target.Player, spawnPos, 4f);
		}
	}
	public void SpawnEnemy(GameObject _prefab, Enemy.Target _target, Vector3 _pos, float _radius){
		if (player == null) {
			SetupPlayer ();
		}
		if (!player.isLocalPlayer) {
			return;
		}
		objToSpawn = _prefab;
		objToSpawn.GetComponent<Enemy> ().target = _target;
		spawnPos = UpdateSpawnPos (_pos, _radius);
		if (isServer) {
			GameObject _obj = (GameObject)Instantiate (objToSpawn, spawnPos, Quaternion.identity);
			NetworkServer.Spawn (_obj);
			Enemy enemy = _obj.GetComponent<Enemy> ();
			EnemyManager.RegisterEnemy (_obj.GetComponent<NetworkIdentity> ().netId.ToString (), enemy);
			enemy.target = Enemy.Target.Player;
		} else {
			CmdSpawnEnemy ();
		}
	}
	[Command]
	private void CmdSpawnEnemy(){
		GameObject _obj = (GameObject)Instantiate (objToSpawn, spawnPos, Quaternion.identity);
		NetworkServer.Spawn (_obj);
		Enemy enemy = _obj.GetComponent<Enemy> ();
		EnemyManager.RegisterEnemy (_obj.GetComponent<NetworkIdentity> ().netId.ToString (), enemy);
		enemy.target = Enemy.Target.Player;
	}
}
