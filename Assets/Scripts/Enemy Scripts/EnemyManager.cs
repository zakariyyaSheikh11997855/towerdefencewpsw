﻿using UnityEngine;
using System.Collections.Generic;

/*
 * Maily keeps track of the enemies - and also match settings
 * Since there is only one Game manger instance per scene, this has been made as a singleton
 * 
*/
public class EnemyManager : MonoBehaviour {

	public static EnemyManager singleton; 


	void Awake()
	{
		if (singleton != null) {
			Debug.LogError ("More than one Enemy manager is running"); 
		} else {
			singleton = this; 
			//singletons make it very easy to gain access to the Gamemanager without any gameobject.find or similar things. 
		}
	}



	/// <summary>
	/// region are easy to keep track of sections of code - the first region here is for
	/// the enemy code
	/// </summary>
	#region enemy tracking 
	private const string ENEMY_ID_PREFIX = "Enemy "; 

	private static Dictionary<string, Enemy> enemies = new Dictionary<string, Enemy>();


	/*
	 * This is so each enemy adds the id from network idenity to its name so that
	 * it is displayed. RegisterEnemy can be used to create a dictionary of all the
	 * enemies in the session
	 * 
	*/
	public static void RegisterEnemy(string _netID, Enemy _enemy)
	{
		string _enemyID = ENEMY_ID_PREFIX + _netID; 
		enemies.Add (_enemyID, _enemy);
		_enemy.transform.name = _enemyID; //sets the name of the game object to the ID
	}

	public static void DeRegisterEnemy(string _enemyID)
	{
		enemies.Remove (_enemyID);
	}

	//you can get a enemy from this getter
	public static Enemy GetEnemy(string _enemyID)
	{
		return enemies[_enemyID];
	}

	//Find the closest enemy to position
	public static Enemy GetClosestEnemy(Vector3 position){
		Enemy currentClosest = default(Enemy);
		float currentClosestDistance = 0f;
		foreach (Enemy enemy in enemies.Values) {
			float distance = Vector3.Distance (enemy.gameObject.transform.position, position);
			if(distance < currentClosestDistance || currentClosestDistance == 0f){
				currentClosest = enemy;
				currentClosestDistance = distance;
			}
		}
		if (currentClosest != default(Enemy)) {
			return currentClosest;
		}
		Debug.LogError ("No enemy found. Returned default enemy likely breaking script using this method");
		return default(Enemy);
	}

	#endregion



}
